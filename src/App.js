import React, { Component } from 'react';

import { CKEditor } from '@ckeditor/ckeditor5-react';

import BalloonEditor from '@ckeditor/ckeditor5-editor-balloon/src/ballooneditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import MathType from '@wiris/mathtype-ckeditor5/src/plugin';

BalloonEditor.builtinPlugins = [
  Essentials,
  Bold,
  Italic,
  Paragraph,
  MathType,
];

const items = ['bold', 'italic', 'MathType'];

BalloonEditor.defaultConfig = {
  language: 'pt-br',
  toolbar: { items: items },
  blockToolbar: items
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <h2>CKEditor 5</h2>
        <CKEditor
          editor={BalloonEditor}
          config={{
            placeholder: 'Digite aqui',
            link: { addTargetToExternalLinks: true },
            removePlugins: ['Title', 'MediaEmbed'],
          }}
          data="<p>Utilizando o ckEditor 5 com build personalizado</p>"
          onReady={editor => {
            // You can store the "editor" and use when it is needed.
            console.log('Editor is ready to use!', editor);
          }}
          onChange={(event, editor) => {
            const data = editor.getData();
            console.log({ event, editor, data });
          }}
          onBlur={(event, editor) => {
            console.log('Blur.', editor);
          }}
          onFocus={(event, editor) => {
            console.log('Focus.', editor);
          }}
        />
      </div>
    );
  }
}

export default App;